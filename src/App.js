import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Gradient } from 'react-gradient';
import { useMediaQuery } from '@mui/material';
import Grid from '@mui/material/Grid';


    export default function SimpleMediaQuery() {
      const matches = useMediaQuery('(min-width:600px)');

      return( <span>{`(min-width:600px) matches: ${matches}`}
    
      <Card style={{margin:0,padding:0,display:'flex',width:540,flexDirection:'column',height:500,
    }}>    
    
   <Card style={{display:'flex', alignItem:'center',flexDirection:'column',marginBottom:-1, marginTop:20,
  borderColor:'white', borderRadius:11,borderTopRightRadius:4,borderBottomLeftRadius:2,flexGrow:1,flexBasis:140,maxWidth:470,flexWrap:'wrap',marginLeft:31}}>
        
      <Gradient style={{flexGrow:1,}}
          gradients = {[
            ['#9b2dbd','#9b2dbd'],
            ]}> 
               
      <CardContent>

        <Typography sx={{ fontSize: 18 }} color="white" gutterBottom
        style={{fontFamily:'"Futura",Bold',fontSize:16, }} >
         
          Wallet Balance  
        </Typography>

        <Typography variant="h6" component="div"
        sx={{fontSize: 31}}
        style={{color:'white'
      ,fontFamily:'"Futura",bold' }}>

         $100,00.<small>22</small> <typography style={{fontSize:15,}}>CAD </typography>
         
        </Typography>

        <Typography style={{float: 'right', marginBottom: 10,
       color:'white' ,fontFamily: '"Futura",bold',}}
        sx={{fontSize: 14, }}>
            
          Lifetime Spending: $250.22
          
        </Typography>
        </CardContent>
        </Gradient>
        </Card>

        <Card style={{backgroundColor: '#1c1240',textAlign:'center',display:'flex', borderTopRightRadius:12, borderTopLeftRadius:15,marginTop:-33,padding:0,flexGrow:1, width:470,marginLeft:31,flexWrap:'wrap'}}>
      <Gradient style={{}}
          gradients = {[
            ['#1c1240','#1c1240'],
            ]} >
     
        <Typography style={{ color:'white',fontWeight:'bold',display:'flex', justifyContent:'center', marginTop:11,
        fontSize:16,}}> 
          Amount to Deposit
          </Typography> 

          <Grid style={{display:'inline-flex', flexDirection:'row',  gap:22,marginTop:11,flexFlow:'row',}}>

          <Button variant="outlined" style={{ display:'flex',backgroundColor:'white',justifyContent:'center',
          height: 25, width: 110, color:'#9b2dbd',fontFamily:'"futura",bold',Size:13,padding:0}}> $20 </Button>
         
          <Button variant="outlined"  style={{
        fontSize: 15, height: 25, width: 110,color: 'white',fontFamily:'"futura",bold', backgroundColor:'#9b2dbd' ,padding:0,
        }}> 
         
          $40
          
          </Button> 
         
          <Button variant="outlined" style={{display:'flex',height: 25, width: 110,backgroundColor:"white", color:'#9b2dbd',padding:0,fontFamily:'"futura",bold',fontSize:13,}}>$60</Button>
           
           </Grid>

         <Grid style={{display:'inline-flex',gap:22,marginTop:11,flexFlow:'row',}}>
           
          <Button variant="outlined" style={{display:'flex',height: 25, width: 110,color:'#9b2dbd',backgroundColor:"white" , borderColor:'white',fontFamily:'"futura",bold',fontSize:12.5,padding:0}}>$80</Button> 

          <Button variant="outlined" style={{display:'flex',height: 25, width: 110,color:'#9b2dbd',backgroundColor:"white",borderColor:'white',fontFamily:'"Futura",bold',fontSize:13,padding:0}}>$100</Button> 
        
          <Button variant="outlined" style={{display:'flex',height: 25, width: 110,color:'#9b2dbd',backgroundColor:"white", borderColor:'white',fontFamily:'"Futura",bold',fontSize:13,padding:0}}>$200</Button>

          </Grid>
           
          <Typography style={{display:'flex',justifyContent:'center' ,marginTop:10, color:'white',fontFamily:'"Futura",light', fontSize:13,}}> Or </Typography>

          <Typography style={{
          fontFamily:"Futura",fontSize:14 , color:'white',display:'flex', justifyContent:'center',marginTop:7 }}>
         Enter an amount: <input type="text" name="Name" size="10" style={{borderRadius:5,}}></input>
        
         </Typography>    
         </Gradient>
         </Card>

         <Card style={{ display:'flex',textAlign:'center',flexDirection:'column',padding:0,flexGrow:1,width:470,marginLeft:31,height:110, marginBottom:22,flexWrap:'wrap'
  }}>
     
    <Typography style={{fontFamily: '"Futura", light',display:'flex', justifyContent:'flex-start', marginLeft:15  }}>
      Purchase Amount:
    </Typography>
    
     <Typography style={{display:'flex',justifyContent:'center',fontFamily: '"Futura" , Bold', fontSize:34,  }}>
       $40.00 CAD
     </Typography>

     <Button variant="outlined"  style={{display:'flex',color: '#9b2dbd',textAlign:'center', borderColor:'#9b2dbd',borderWidth:3.2, fontSize:13, fontFamily:'"Futura",light',  textTransform:'none',marginTop:21, borderRadius:7
    }}>
         Choose Payment Method
    
    </Button>

    </Card>
    </Card>
    </span>
 );      
} 
